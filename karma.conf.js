//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './app',

    reporters: ['progress', 'junit'],

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'components/**/*.js',
      'view*/**/*.js'
    ],

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-junit-reporter'
    ],

    junitReporter: {
      outputDir: '../',
      outputFile: 'test_out/unit.xml',
      useBrowserName: false,
      suite: 'unit'
    }

  });
};
